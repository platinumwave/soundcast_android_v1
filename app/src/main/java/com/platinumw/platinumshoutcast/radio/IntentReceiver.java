package com.platinumw.platinumshoutcast.radio;

import android.content.Context;
import android.content.Intent;

/**
 * Created by platinumlankapvtltd on 12/28/16.
 */

public class IntentReceiver extends android.content.BroadcastReceiver  {

    public void onReceive(Context ctx, Intent intent) {
        if (intent.getAction().equals(
                android.media.AudioManager.ACTION_AUDIO_BECOMING_NOISY)) {
            Intent myIntent = new Intent(ctx, MediaPlayerService.class);
            ctx.stopService(myIntent);
        }
    }
}
